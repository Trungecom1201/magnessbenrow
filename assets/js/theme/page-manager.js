import utils from '@bigcommerce/stencil-utils';
import $ from 'jquery';


export default class PageManager {
    constructor(context) {
        this.context = context;
        $(document).ready(function(){
            $('body').delegate('.previewCart .mini-cart-continue-shopping','click', function() {
                $('body').removeClass("body-show-mini-cart");
                $('.mask-mini-cart').removeClass("mask-body");
                $('body').removeClass("body-show-mini-cart-mobile");
                $('body').removeClass("body-open-filter-mobile");
                $('#cart-preview-dropdown').removeClass("show-product-cart");
                $('.mask-mini-cart').removeClass("mask-body-mobile");
            });
        });
            var self = this;
            $('body').delegate( "#cart-preview-dropdown .cart-remove", "click", function() {
                var itemId = $(this).attr('data-cart-itemid');
                self.cartRemoveItem(itemId);
            });

        var currentObject = this;
        $(".update-qty-product-cart #update-cart-reload").click(function(){
            currentObject.updateDataAllCart(currentObject);
            return false;
        });

        var currentObject = this;
        $( "body" ).delegate( ".cart .cart-item-quantity input", "change", function() {
            currentObject.updateDataAllCart(currentObject);
        });

        $( "body" ).delegate( ".cart-item-quantity button", "click", function() {

            if($(this).hasClass('plus-product')){
                var itemId = $(this).attr('data-cart-itemid');
                var newQty1 = parseInt($(this).val());
                var newQty = newQty1 + 1;
                $(this).prev().val(newQty);
                $(this).prev().change();

                var data = [];
                data['id'] = itemId;
                data['qty'] = newQty;
            }
            if($(this).hasClass('mins-product')){
                var itemId = $(this).attr('data-cart-itemid');
                var newQty1 = parseInt($(this).val());
                var newQty = newQty1 - 1;
                $(this).next().val(newQty);
                $(this).next().change();

                var data = [];
                data['id'] = itemId;
                data['qty'] = newQty;
            }

        });


    }

     cartRemoveItem(itemId) {
        var self = this;
        utils.api.cart.itemRemove(itemId, (err, response) => {
            self.updateMiniCartContent();
    });
    }

    updateCountItemsMiniCart(){
        var updateQty = 0;
        $('#cart-preview-dropdown input').each(function(index, value){
            var object = $(this);
            updateQty += parseInt(object.val());
            console.log(object.val());
        });
        $('.mini-cart-qty-dropdown .cart-quantity').text(updateQty);
        $('.top-header .top-link-account .cart-quantity').text('('+ updateQty + ' ' + 'item' + ')');


    }

     updatecartData(itemId,newQty){
        var self = this;
        utils.api.cart.itemUpdate(itemId, newQty, (err, response) => {
            if(response.data.status !== "failed"){
            self.updateMiniCartContent();
        }else{
            /*alert('ERROR: Quanitity out of stock or unavailable');*/
        }

    });
    }

     updateMiniCartContent(){
        var self = this;
        utils.api.cart.getContent({template: 'common/cart-preview'}, (err, response) => {
            $('#cart-preview-dropdown').html(response);
        self.updateCountItemsMiniCart();
    });
    }

    //=============================================================

    updateDataAllCart(currentObject){
        window.items = [];
        $('.cart-item-quantity .form-input--incrementTotal').each(function(index){
            var itemId = $(this).attr('data-card');
            var newQty = $(this).val();

            if(index === 0){
                currentObject.updatecartData(itemId,newQty);
            }
            var data = [];
            data['id'] = itemId;
            data['qty'] = newQty;
            window.items[index] = data;
        });
    }


    /**
     * Function update all items qty to cart
     * @param itemId
     * @param newQty
     * itemId : cart item id
     * newQty : qty item should be update
     *
     * **/
    updatecartData(itemId,newQty){

        var currentObject = this;
        utils.api.cart.itemUpdate(itemId, newQty, (err, response) => {
            // checking data item make sure always update all items before update again cart page
            if(window.items.length > 0){
                var i = 0;
                var index = 0;
                for(i; i < window.items.length; i++){
                    if(index === 0){
                        var data = window.items[i];
                        if(typeof data !== "undefined"){
                            currentObject.updatecartData(data['id'],data['qty']);
                            window.items.splice(i,1);
                            index++;
                        }

                    }

                }
            }else {
                // update content cart page
                if (this.refreshContent) // check if `this` refers to cart
                    this.refreshContent(false);
            }

            // Display error message
            if (response.data.status === 'failed') {
                const element = '<input class="swal-cart-quantity-update" type="hidden" ' +
                            'status="'+ response.data.status +'"' +
                            'message="'+ response.data.errors.join('\n') +'" />';
                $('body').append(element);
            }

    });
    }



    type() {
        return this.constructor.name;
    }

    onReady() {

    }

    static load(context) {
        const page = new this(context);


        $(document).ready(() => {
            page.onReady.bind(page)();

        });
    }
}
