import { hooks } from '@bigcommerce/stencil-utils';
import CatalogPage from './catalog';
import $ from 'jquery';
import FacetedSearch from './common/faceted-search';
import mrCategory from './es/theme';

export default class Category extends CatalogPage {
    onReady() {
        if ($('#facetedSearch').length > 0) {
            this.initFacetedSearch();
        } else {
            this.onSortBySubmit = this.onSortBySubmit.bind(this);
            hooks.on('sortBy-submitted', this.onSortBySubmit);
        }
        let $cmsContainer3 = $('.latest_editorials');

        $.ajax({
            url: '/latest-editorials/',
            type: 'GET',
            dataType: 'html'
        }).done(function(data) {
            var pageContent3 = $('.latest_editorials_container', data);
            $cmsContainer3.html( pageContent3.html() );
        });
    }

    after(next){
        next();
    }

    initFacetedSearch() {
        const $productListingContainer = $('#product-listing-container');
        const $facetedSearchContainer = $('#faceted-search-container');
        const productsPerPage = this.context.categoryProductsPerPage;
        const requestOptions = {
            config: {
                category: {
                    shop_by_price: true,
                    products: {
                        limit: productsPerPage,
                    },
                },
            },
            template: {
                productListing: 'category/product-listing',
                sidebar: 'category/sidebar',
            },
            showMore: 'category/show-more',
        };

        this.facetedSearch = new FacetedSearch(requestOptions, (content) => {
        $productListingContainer.html(content.productListing);
        $facetedSearchContainerTop.html(content.sidebarTop);
        $facetedSearchContainer.html(content.sidebar);
        $facetedSearchContainer.prepend(content.sidebarToggle);

        // HaloThemes function
        setActiveCategory();
        productDisplayMode();
        haloProductImageSwap();
        sidebarToggleMobile();

        //$('html, body').animate({
        //  ,
        //}, 3000);
    });
    }


}
