import $ from 'jquery';
import 'foundation-sites/js/foundation/foundation';
import 'foundation-sites/js/foundation/foundation.dropdown';
import utils from '@bigcommerce/stencil-utils';
import ProductDetails from '../common/product-details';
import { defaultModal } from './modal';
import 'slick-carousel';

export default function (context) {
    const modal = defaultModal();

    $('body').on('click', '.quickview', event => {
        event.preventDefault();


        const productId = $(event.currentTarget).data('productId');

        modal.open({ size: 'large' });

        utils.api.product.getById(productId, { template: 'products/quick-view' }, (err, response) => {
            modal.updateContent(response);

            modal.$content.find('.productView').addClass('productView--quickView');

            $('.productView--quickView .right-slide').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade:true,
                asNavFor: '.productView--quickView .left-slide'
            });

            $('.productView--quickView .left-slide').slick({
                slidesToShow: 4,
                arrows: false,
                slidesToScroll: 1,
                asNavFor: '.productView--quickView .right-slide',
                dots: false,
                focusOnSelect: true
            });
            
            /**
             * Enable submit button if size and color is selected
             */
            const checkRequiredFields = function() {
                const radio = $('.productView--quickView form[data-cart-item-add] .set-rectangle input[type="radio"]');
                const submit = $('.productView--quickView #form-action-addToCart');
                debugger;
                if ( radio.length ) {
                    radio.each(function (e) {
                        if ($(this).is(':checked')) {
                            if ($(this).is(':checked'))
                                submit.prop('disabled', false); 
                            else
                                submit.prop('disabled', true);
                            return false;
                        }
                    })
                } else {
                    submit.prop('disabled', false);
                }
            }();

            const checkRequiredFieldsHander = function () {
                const label = $('.productView--quickView form[data-cart-item-add] .set-rectangle label.form-option');
                const radio = $('.productView--quickView form[data-cart-item-add] .set-rectangle input[type="radio"]');
                label.on('click', function() {
                    $('.productView--quickView form[data-cart-item-add] .set-rectangle').find('#' + $(this).attr('for')).trigger('click');
                    const submit = $('.productView--quickView #form-action-addToCart');
                    if ( radio.length ) {
                        radio.each(function (e) {
                            if ($(this).is(':checked')) {
                                if ($(this).is(':checked'))
                                    submit.prop('disabled', false); 
                                else
                                    submit.prop('disabled', true);
                                return false;
                            }
                        })
                    } else {
                        submit.prop('disabled', false);
                    }
                })
            }();

            $(".productView--quickView").find('#form-action-addToCart').click(function(){
                setTimeout(function(){
                    modal.close();
                    $(".navUser .navUser-section .navUser-item--cart .mini-cart-qty-dropdown").trigger('click');
                }, 2000);
            });

            return new ProductDetails(modal.$content.find('.quickView'), context);
        });
    });
}

$('#modal').ajaxComplete(function( event, request, settings ) {
    var firstColor = $('.swatch .form-radio');
    firstColor.each(function () {
        $(firstColor[0]).trigger('click');
    });
});
