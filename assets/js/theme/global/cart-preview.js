import $ from 'jquery';
import 'foundation-sites/js/foundation/foundation';
import 'foundation-sites/js/foundation/foundation.dropdown';
import utils from '@bigcommerce/stencil-utils';

export const CartPreviewEvents = {
    close: 'closed.fndtn.dropdown',
    open: 'opened.fndtn.dropdown',
};

export default function () {
    const loadingClass = 'is-loading';
    const $cart = $('[data-cart-preview]');
    const $sign = $('[data-sign]');
    const $cartDropdown = $('#cart-preview-dropdown');
    const $cartLoading = $('<div class="loadingOverlay"></div>');

    $('body').on('cart-quantity-update', (event, quantity) => {
        $('.cart-quantity')
            .text(quantity)
            .toggleClass('countPill--positive', quantity > 0);
    });

    $cart.on('click', event => {
        const options = {
            template: 'common/cart-preview',
        };

        // Redirect to full cart page
        //
        // https://developer.mozilla.org/en-US/docs/Browser_detection_using_the_user_agent
        // In summary, we recommend looking for the string 'Mobi' anywhere in the User Agent to detect a mobile device.
        /*if (/Mobi/i.test(navigator.userAgent)) {
            return event.stopPropagation();
        }*/

        event.preventDefault();

        $cartDropdown
            .addClass(loadingClass)
            .html($cartLoading);
        $cartLoading
            .show();

        utils.api.cart.getContent(options, (err, response) => {
            $cartDropdown
                .removeClass(loadingClass)
                .html(response);
            $cartLoading
                .hide();
        });
    });

    const eventtype = $.browser.mobile ? 'touchstart' : 'click';
    $cart.on(eventtype, function(ev) {
        if(!$('.logo-search-header .main-header__cart-account .navUser-item--cart .subnav-mini-cart').hasClass('is-open')){
            ev.preventDefault();
            const options = {
                template: 'common/cart-preview',
            };
            $(this).parent().addClass('is-open');
            $(this).attr('aria-expanded', true);
            $(this).next().attr('aria-hidden', false);
        }else {
            $(this).parent().removeClass('is-open');
            $(this).attr('aria-expanded', false);
            $(this).next().attr('aria-hidden', true);
        }


        $cartDropdown
            .addClass(loadingClass)
            .html($cartLoading);
        $cartLoading
            .show();

        utils.api.cart.getContent(options, (err, response) => {
            if (response.search('previewCart-emptyBody') > 0) {
            $('#cart-preview-dropdown .triangle-with-shadow').removeClass('triangle-grey');
        } else {
            $('#cart-preview-dropdown .triangle-with-shadow').addClass('triangle-grey');
        }

        $cartDropdown
            .removeClass(loadingClass)
            .html(response);
        $cartLoading
            .hide();

        const $previewCartList = $('.previewCartList');
        $previewCartList.mCustomScrollbar('destroy');
        if ($previewCartList.length) {
            $previewCartList.mCustomScrollbar({
                scrollInertia: 400,
            });
        }
    });
    });

    $(document).on(eventtype, function(ev) {
        if ($(ev.target).closest('#top-cart').length === 0) {
            $cart.parent().removeClass('is-open');
            $cart.attr('aria-expanded', false);
            $cart.next().attr('aria-hidden', true);
        }
    });

}
