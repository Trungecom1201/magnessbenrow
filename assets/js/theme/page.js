import PageManager from './page-manager';

export default class Page extends PageManager {

	onReady() {
		$('.table-mb-main').slick({
			  infinite: true,
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  arrows: false,
				dots:true
			});
	}
}
