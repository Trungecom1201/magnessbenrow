import PageManager from './page-manager';
import Instafeed from 'instafeed.js';
import $ from 'jquery';

export default class Home extends PageManager {
    constructor(context) {
        super(context);

        this.initInstagramFeed();
        this.heroSlideActionHandler();
    }

    onReady() {
        let $cmsContainer8 = $('.new-products-section');



        $.ajax({
            url: '/appliances/fridges-freezers',
            type: 'GET',
            dataType: 'html'
        }).done(function(data) {
            var pageContent8 = $(data);
            $cmsContainer8.html( pageContent8.html() );
            $('.productCarousel-new').slick()
        }); // end ajax done

        // temporary fix for featured product
        if($(".mobileNewArrivals .product").length%2==1){
            ($(".mobileNewArrivals .product:last").clone()).appendTo($(".productGrid.productGrid--maxCol4.mobileNewArrivals"));
        }
    }

    /*
     * Instagram feeds on homepage
     * Fetch ig feed from user account
     */
    initInstagramFeed() {
        const USER_ID = '8887670470',
            CLIENT_ID = '75e20395da8444149308f2693f0e6846',
            ACCESS_TOKEN = '8887670470.75e2039.16f841dc9965453798d08287344f0cec';

        var feed = new Instafeed({
            get: 'user',
            userId: USER_ID,
            clientId: CLIENT_ID,
            accessToken: ACCESS_TOKEN,
            resolution: 'standard_resolution',
            target: 'el-instafeed',
            template: '<li>' + 
                        '<div class="feed-container">' +
                            '<a href="{{link}}" target="_blank" id="{{id}}">' +
                                '<div class="feed-img">' +
                                    '<span class="overlay-img"><img src="{{image}}" /></span>' +
                                    '<span class="feed-img-container"><img src="{{image}}" /></span>' +
                                '</div>' +
                                '<div class="feed-info">' +
                                    '<div class="feed-info-content">' +
                                        '<div class="feed-meta">' +
                                            '<div class="feed-info-meta">' +
                                                '<svg class="likes-icon" viewBox="0 0 24 24"><path d="M17.7,1.5c-2,0-3.3,0.5-4.9,2.1c0,0-0.4,0.4-0.7,0.7c-0.3-0.3-0.7-0.7-0.7-0.7c-1.6-1.6-3-2.1-5-2.1C2.6,1.5,0,4.6,0,8.3c0,4.2,3.4,7.1,8.6,11.5c0.9,0.8,1.9,1.6,2.9,2.5c0.1,0.1,0.3,0.2,0.5,0.2s0.3-0.1,0.5-0.2c1.1-1,2.1-1.8,3.1-2.7c4.8-4.1,8.5-7.1,8.5-11.4C24,4.6,21.4,1.5,17.7,1.5z M14.6,18.6c-0.8,0.7-1.7,1.5-2.6,2.3c-0.9-0.7-1.7-1.4-2.5-2.1c-5-4.2-8.1-6.9-8.1-10.5c0-3.1,2.1-5.5,4.9-5.5c1.5,0,2.6,0.3,3.8,1.5c1,1,1.2,1.2,1.2,1.2C11.6,5.9,11.7,6,12,6.1c0.3,0,0.5-0.2,0.7-0.4c0,0,0.2-0.2,1.2-1.3c1.3-1.3,2.1-1.5,3.8-1.5c2.8,0,4.9,2.4,4.9,5.5C22.6,11.9,19.4,14.6,14.6,18.6z"></path></svg>' +
                                                '<div class="count">{{likes}}</div>' +
                                            '</div>' +
                                            '<div class="feed-info-meta">' +
                                                '<svg class="comments-icon" viewBox="0 0 24 24"><path d="M1,11.9C1,17.9,5.8,23,12,23c1.9,0,3.7-1,5.3-1.8l5,1.3l0,0c0.1,0,0.1,0,0.2,0c0.4,0,0.6-0.3,0.6-0.6c0-0.1,0-0.1,0-0.2l-1.3-4.9c0.9-1.6,1.4-2.9,1.4-4.8C23,5.8,18,1,12,1C5.9,1,1,5.9,1,11.9z M2.4,11.9c0-5.2,4.3-9.5,9.5-9.5c5.3,0,9.6,4.2,9.6,9.5c0,1.7-0.5,3-1.3,4.4l0,0c-0.1,0.1-0.1,0.2-0.1,0.3c0,0.1,0,0.1,0,0.1l0,0l1.1,4.1l-4.1-1.1l0,0c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.3,0.1l0,0c-1.4,0.8-3.1,1.8-4.8,1.8C6.7,21.6,2.4,17.2,2.4,11.9z"></path></svg>' +
                                                '<div class="count">{{comments}}</div>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="caption">{{caption}}</div>' +
                                    '</div>' +
                                '</div>' +
                            '</a>' +
                        '</div>' +
                    '</li>',
            sortBy: 'most-recent',
            limit: 6,
            links: false,
            success: (response) => { 
                // console.log('[InstafeedJS] Success!'); 
                const handle = response.data[0].user.username;
                const link = $('.el-instafeed-container').find('.ig-handle');
                link.attr('href', 'https://www.instagram.com/' + handle + '/').html('@' + handle);
            },
            after: (data) => {
                // console.log('[InstafeedJS] Instagram feed loaded');
                let group = $('<ul class="img-group"></ul>')
                $('#el-instafeed > li').each(function(i) {
                    return i > 1 ? $(this).appendTo(group) : null;
                })
                group.appendTo($('<li></li>').appendTo($('#el-instafeed')));
                $('.el-instafeed-container').addClass('loaded');
            }
        });
        feed.run();
    }

    /*
     * Make buttons in homepage carousel clickable
     */
    heroSlideActionHandler() {
        const carouselItem = $('.heroCarousel .slick-slide');

        carouselItem.each( function() {
            const $item = $(this);
            $item.on('click', function(e) {
                e.preventDefault();
            });

            const $button = $item.find('.heroCarousel-action');
            const link = $item.attr('href') || window.location.href;

            if ( $button.length ) {
                $button.on('click', () => window.location.href = link );
            } else {
                $item.on('click', () => window.location.href = link );
            }

        });
    }
    onReady() {
        let $cmsContainer1 = $('.shop-editorial');
        let $cmsContainer2 = $('.season-collection');

        $.ajax({
            url: '/shop-editorial',
            type: 'GET',
            dataType: 'html'
        }).done(function(data) {
            var pageContent1 = $('.editorial-container', data);
            $cmsContainer1.html( pageContent1.html() );
        });
        $.ajax({
            url: '/season-collection',
            type: 'GET',
            dataType: 'html'
        }).done(function(data) {
            var pageContent2 = $('.collection-container', data);
            $cmsContainer2.html( pageContent2.html() );
        });
    }
}
