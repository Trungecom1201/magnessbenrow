import $ from 'jquery';
import utils from "@bigcommerce/stencil-utils";
import swal from "sweetalert2";
import Url from 'url';
import urlUtils from '../common/url-utils';

export default function(){
    scrollHeader();
    isLoading();
    showSearch();
    slideFooter();
    megaMenuMobile();
    showMenuAccount();
    prevNextSliderProduct();
    showTabcontent();
    showSizeGuidePopUp();
    menuCategorySearch();
    fist_session_banner();
    categoryPage();
    showMenuCategoryFilter();
    mrSubmitSubcribe();
    productViewslide();
    addToCartShowMiniCart();
    addFeedOnIgPage();
    bindEventforPagination();
    openSubscriptionModal();
    changeCategoryColorFilter();
    returnsPage();
    loadItem();
    showMenu();
    sliderBannerHome();
    hoverMenuActive();
    hoverMenuActiveScroll();
    $('.navPages-container .navPages-list-menu .navPage-subMenu:first').addClass('active');
    $('.navPages-container .navPages-list-nav .navPages-action:first').addClass('active');
    $('.search-form-menu .navPages-list-menu .navPage-subMenu:first').addClass('active');
    $('.search-form-menu .navPages-list-nav .navPages-action:first').addClass('active');
    if ( /^((?!chrome|android).)*safari/i.test(navigator.userAgent)) { $('body').addClass('is-safari');}
    }



function scrollHeader(){



    // Hide Header on on scroll down
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;


    $(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    },1);

    function hasScrolled() {
        var st = $(window).scrollTop();

        // Make sure they scroll more than delta
        if(Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.

        var heightBody = $("body").height();
        var height20 = (heightBody/100)*20;

        if (st > lastScrollTop){
            // Scroll Down
            $(".header").addClass("active-header");
            //$(".header").css("position", "fixed");
            //$(".body").css("padding-top", "40px");
            if ($('.sign-in-header .container-sign-in').hasClass('is-open')) {
                $('.sign-in-header .container-sign-in').removeClass('is-open');
                $('.sign-in-header .active-show-sign-in').attr('aria-expanded', false);
                $('.sign-in-header .container-sign-in').attr('aria-hidden', true);
            }
            if ($('.main-header__cart-account .navUser-item--cart .subnav-mini-cart').hasClass('is-open')) {
                $('.main-header__cart-account .navUser-item--cart .subnav-mini-cart').removeClass('is-open');
                $('.main-header__cart-account .navUser-item--cart .mini-cart-qty-dropdown').attr('aria-expanded', false);
                $('.main-header__cart-account #cart-preview-dropdown').attr('aria-hidden', true);
            }
            if(st < height20){
                $(".header").addClass("active-header");
            }

        }else {
            if(st + $(window).height() < $(document).height()) {
                $(".header").removeClass("active-header");
                var stt = $(window).scrollTop();

                if(stt >= height20){
                    //$(".header").css("position", "fixed");
                    //$(".body").css("padding-top", "20px");
                    $(".header .header-container .navPages-container").hide();
                    $(".header").removeClass("active-header");
                    $(".header").addClass('top-header-nav');
                    $(".header .header-container .logo-search-header .dropdown--quickSearch").addClass('active-search-srcoll');
                    $(".header .header-container .navMenu-toggle-scroll").show();
                }else{
                    //$(".header").css("position", "sticky");
                    //$(".body").css("padding-top", "0px");
                    $(".header").removeClass("active-header");
                    $(".header .header-container .navPages-container").show();
                    $(".header").removeClass('top-header-nav');
                    $(".header .header-container .logo-search-header .dropdown--quickSearch").removeClass('active-search-srcoll');
                    $(".header .header-container .navMenu-toggle-scroll").hide();
                    var scroll2 = $(window).scrollTop();
                    if(scroll2 < height20){
                        $(".header").removeClass("active-header");
                    }
                }
            }

        }
        lastScrollTop = st;
    }



}

function isLoading(){

    const loadingClass = 'is-loading';
    const $cartDropdown = $('.container-product-television .container-product #product-category');
    const $cartLoading = $('<div class="loadingOverlay"></div>');
    const $cartLoadingProduct = $('#product-category');
    $cartDropdown
        .addClass(loadingClass)
        .html($cartLoading);
    $cartLoading
        .show();
    $cartLoadingProduct
        .show();


    $.ajax({
        url: "/tvs-technology/tvs",
        type:'GET',
        success: function(data) {

            var thishtml =  $(data).find('.productGrid').html();
            $('#product-category').html(thishtml).slick({
                infinite: false,
                dots: false,
                slidesToShow: 5,
                slidesToScroll: 1,
                arrows: true,
                centerPadding: '60px',
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToScroll: 1,
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 767,
                        settings: {
                            slidesToScroll: 1,
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 550,
                        settings: {
                            slidesToScroll: 1,
                            slidesToShow: 1
                        }
                    }
                ]
            });


        }
    });

}

function productViewslide() {

    $('.product-page-wear .productCarousel').slick({
        infinite: true,
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 2,
        arrows: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.product-page-recently-viewed .productCarousel').slick({
        infinite: true,
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 2,
        arrows: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 1
                }
            }
        ]
    });
}
function showSearch(){
    $(document).ready(function () {
        $(".icon-search").click(function () {
            if($(window).width() < 768) {
                $("#quickSearch-mobile").slideToggle();
                $("#menu .navPages-quickSearch").hid();
            } else {
                $("#quickSearch-mobile").hide();
                $("#menu .navPages-quickSearch").slideToggle();
            }
        });

        $('#FormField_25 #FormField_25_day').change(function() {
            $("#FormField_25  .form-label").addClass('birthday');
        });
    });
}

$('.pages-auth-create-account #FormField_11_select').val('');
$('.pages-auth-create-account #FormField_25_day').val('');
$('.pages-auth-create-account #FormField_25_month').val('');
$('.pages-auth-create-account #FormField_25_year').val('');
$('.page-add-address #FormField_11 #FormField_11_select').val('');

function slideFooter(){

    $('.slick-category-list').not('.slick-initialized').slick({
        dots: false,
        loop: true,
        infinite: false,
        mobileFirst: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        prevArrow: $('.swiper-button-prev'),
        nextArrow: $('.swiper-button-next'),
        responsive: [
            {
                breakpoint: 1023,
                settings: {
                    slidesToScroll: 2,
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToScroll: 2,
                    slidesToShow: 3
                }
            }
        ]
    });
}

function megaMenuMobile(){

    $(window).click(function(e) {
        var container = $('.navPages-container .navPages-item .navPage-subMenu.show');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.slideUp().removeClass('show');
        }
    });

    $('.fabric_care_product .header_fabric_care').click(function(){
       $('.fabric_care_product .content_fabric_care').slideToggle();
        $('.fabric_care_product .header_fabric_care img').toggleClass("img-content");
    });

    $('.delivery_product .header_delivery').click(function(){
       $('.delivery_product .content_delivery').slideToggle();
        $('.delivery_product .header_delivery img').toggleClass("img-content");
    });
    $('.label_story_product .header_label_story').click(function(){
        $('.label_story_product .content_label_story').slideToggle();
        $('.label_story_product .label_story_delivery img').toggleClass("img-content");
    });
}

function showMenuAccount(){
    //$(".navUser .nav-sign-in-mobile").click(function(){
    //    $(".account-login-error").slideToggle();
    //});
    //
    //$(".navUser .navUser-item--account").click(function(){
    //
    //    if($('.navPages-container').hasClass('is-open')){
    //        $('.mobileMenu-toggle').trigger('click');
    //        $(".drop-menu-sign-in-success").slideToggle();
    //    }else {
    //        $(".drop-menu-sign-in-success").slideToggle();
    //    }
    //});
    //$(".show_menu_account").click(function () {
    //    if($('.navPages-container').hasClass('is-open')){
    //        $('.mobileMenu-toggle').trigger('click');
    //        $(".account-login-error").slideToggle();
    //    }else {
    //        $(".account-login-error").slideToggle();
    //    }
    //})
    //$('.mobileMenu-toggle').click(function () {
    //    if ($('.account-login-error').css('display') == 'block' || $('.drop-menu-sign-in-success').css('display') == 'block') {
    //        $(".account-login-error").slideUp();
    //        $('.drop-menu-sign-in-success').slideUp();
    //    }
    //});
}


function mrSubmitSubcribe() {
    $(".form-subscribe .form").submit(function(e) {

        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function()
            {
                $('.form-subscribe').addClass('success-submit');// show response from the php script.
            }
        });

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
}


function prevNextSliderProduct(){
    $(".product-page-wear .prev-slider-product-page").click(function(){
        $("#tab-related .slick-prev").trigger('click');
    });

    $(".product-page-wear .next-slider-product-page").click(function(){
        $("#tab-related .slick-next").trigger('click');
    });
}

function showTabcontent(){
    $(".tab").click(function () {
        $(this).parent().next().slideToggle();
        $(this).toggleClass('is-open');
    })

}

function showSizeGuidePopUp() {
    $(".size-guide").click(function(){
        $(".size-guide-pop-up").addClass('active');
        $(".mask").addClass('active');
    })
    $("#close-pop-up").click(function () {
        $(".size-guide-pop-up").removeClass('active');
        $(".mask").removeClass('active');
    })

}

function menuCategorySearch(){
    $(".search-form-menu .form .form-input").change(function(){
        $(".search-form-menu .quickSearchResults").toggleClass("show_menu_search");
    });

    $('.navUser .navUser-action--currencySelector').click(function(){
        $('.navUser #currencySelection_aa').slideToggle();
    });

    $(".banner-body-page .close-popup").click(function(){
        $('.banner-body-page').addClass("close-popup-banner");
    });
}

function  categoryPage() {

    // filter category
    $(".toggleClass").click(function () {
        $(this).toggleClass('is-open');
        $(this).next().slideToggle();
    });

    $(".accordion-block .accordion-navigation").click(function(){
        $(this).toggleClass('is-active');
        $(this).next().slideToggle();
    });


    // numbers of products
    $(".numbers-product").on('change',function () {
        var originalURL = window.location.href;
        var href = $(this).val();
        var currentParam =  window.location.search;
        var splitUrl = currentParam.split("&");
        var check1 = splitUrl.includes("limit=24");
        var check2 = splitUrl.includes("limit=48");
        var check3 = splitUrl.includes("limit=96");

        if (check1 === true || check2 === true || check3 === true ){
            var alteredURL = removeParam("limit", originalURL);
            var checkUrl = alteredURL.includes("?");

            if (checkUrl === false) {
                var newParam = alteredURL + "?" + href;
                window.location = newParam;
            } else {
                var newParam = alteredURL + "&" + href;
                window.location = newParam;
            }
        }else{
            if (currentParam === "") {
                var newParam = "?_bc_fsnf=1" + "&" + href;
                window.location = newParam;
            } else {
                var newParam = currentParam + "&" + href;
                window.location = newParam;
            }
        }
    })

        //show filter and move filter mobile
    $("body").delegate( ".open-filter", "click", function() {
        $("body").addClass("body-open-filter-mobile");
        $(".mask-mini-cart").addClass("mask-body")
        $(".move-filter").append($(".filter-move"));

    });
    $(".open-filter").one("click", function () {
            $(".toggleLink").removeClass("is-active");
    });
    $(".close-filter").click(function () {
        $("body").removeClass("body-open-filter-mobile");
        $(".mask-mini-cart").removeClass("mask-body")
    });

    //remove filter
    $("body").delegate( ".rs-filter", "click", function() {
            $(".trigger-remove").trigger('click');
            // window.location = window.location.href;

    });

}

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/* remove cookie by name */
function removeCookie(cname){
    document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
};

function fist_session_banner(){

    var fist_time = sessionStorage.getItem("fist_time");
    if(!fist_time){
        sessionStorage.setItem("fist_time", "1");
        $(".banner-body-page").addClass("show-banner-popup");
    }

    //show banner in brand page
    var banner_brand = $('.background-brand meta').attr('content');
    $('.background-brand').html(banner_brand);

    // remove selected option first time page load
    // $('.actionBar .form-select').find('option:selected').remove();
}


function showMenuCategoryFilter(){
    $(".sidebarBlock.sub-category .accordion-navigation-actions").click(function(){
        $(".sidebarBlock.sub-category .navList").slideToggle();
        $(".sidebarBlock.sub-category .toggleLink-text--on").toggleClass("show_menu_category");
        $(".sidebarBlock.sub-category .toggleLink-text--off").toggleClass("show_menu_category_filter");
    });
    $("body").delegate( "#modal .set-rectangle .form-option", "click", function() {
        $(this).prev().prop('checked', true);
    });
    $("body").delegate( ".swatch .form-option", "click", function() {
            var currentColor = $('.color-name span');
            var currentClick = $(this).children();
            var thisColor = currentClick[0].dataset.title;
            for(var i =0; i < currentColor.length ; i ++) {
                var a = $(currentColor[i]);
                if (thisColor === a[0].innerHTML) {
                    currentColor.removeClass('active');
                    a.addClass('active');
                }
            }
    });
    $('.heroCarousel a').click(function (e) {
        e.preventDefault();
    });
    $('.productGrid #add-wishlist').click(function(event){
        event.preventDefault();
        var url = this.href;
        var self = $(this);
        if(url){
            $.ajax({
                method: "POST",
                url: url,
            })
                .done(function(done) {
                    self.children().find("use").addClass('heart');
                });
        }

    });
}



function addToCartShowMiniCart (){

    $(".pages-product .productView-details #form-action-addToCart").click(function(){
        setTimeout(function(){
            $(".navUser .navUser-section .navUser-item--cart .mini-cart-qty-dropdown").trigger('click');
        }, 2000);
    });
}

/* bind click event for category page pagination to scroll */
function bindEventforPagination() {
    $('main').on('click', '.main-category-footer .pagination-link', function(e) {
        if ( $(this).parent().hasClass('pagination-item--current') ) {
            e.preventDefault();
        } else {
            $('html, body').stop().animate({scrollTop:0}, 500, 'swing');
        }
    });
}

/* Adds ig script if on the instagram page */
function addFeedOnIgPage() {
    var ig = "instagram";
    var path = window.location.pathname.toString().replace(/\//g,"");

    if (ig === path) {
        var script = '<script type="text/javascript" src="https://foursixty.com/media/scripts/fs.embed.v2.5.js" data-feed-id="essential-label" data-open-links-in-same-page="false" data-theme="sizes_v2_5" data-page-size="28"></script>';
        $('main .page-content').append(script);
        $('html').addClass('pages-instagram');
    }
    /* remove empty <p> */
    $('main .page-content p').each(function() {
        var $this = $(this);
        if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
            $this.remove();
    });
}

function openSubscriptionModal() {
    $('.footer-subscribe-btn').on('click', function(e) {
        e.preventDefault();
        openSubsFormModal();
    });
}

/* transform string into hyphened format */
function formatHashString(str) {
    return str.replace(/[\./ ,:-]+/g, " ").replace(/\s\s+/g, ' ').replace(/ +/g, '-').toLowerCase();
}

/* URL encoding */
function encodeURL(str) {
    return encodeURIComponent(str)
    .replace(/!/g,  '%21').replace(/'/g,  '%27').replace(/\(/g, '%28')
        .replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

/* Change category color filter */
function changeCategoryColorFilter(){
    var target = $('#facetedSearch-navList--Color li a');
    if (target) {
        var path = 'https://cdn11.bigcommerce.com/s-uw3pyspwzz/product_images/uploaded_images/';
        var prefix = 'filter-color-';
        var ext = '.jpg';
        target.each(function() {
            var color = $(this).data('color');
            var name = formatHashString(color);
            var img =  $('<img />', {
                class: 'filter-color-' + name,
                src: path + prefix + name + ext,
                alt: color
            });
            $(this).append(img);
        });
        target.closest('#facetedSearch-navList--Color').addClass('color-filter-initialized');
    }
}


function returnsPage() {
    // returns page
    $('.page-return .second_block .but').click(function() {
        $(this).removeClass('active');
    })
    $('.page-return .second_block .but').hover(function(){
        $(this).addClass('active');
    }, function() {
        $(this).removeClass('active');
    });

}


function showMenu(){
    $(".header .navPages-container .container-menu").mouseover(function(){
        $(".mask-mini-cart").addClass('mask-mini-cart-active');

    });
    $(".header .navPages-container .container-menu").mouseout(function(){
        $(".mask-mini-cart").removeClass('mask-mini-cart-active');
        $(".navPages-list .navPages-item:nth-child(1)").removeClass('active');
    });

    $(".header .header-container .logo-search-header .search-form-menu .navMenu-toggle-scroll").mouseover(function(){
        $(".mask-mini-cart").addClass('mask-mini-cart-active');
    });

    $(".header .header-container .logo-search-header .search-form-menu .navMenu-toggle-scroll").mouseout(function(){
        $(".mask-mini-cart").removeClass('mask-mini-cart-active');
    });

    $(".header .header-container .logo-search-header .mobileMenu-toggle").click(function(){
       $(".body").addClass('show-active-menu');
        $(".header").addClass('show-active-menu');
        $("body").addClass('body-active');
        $(".footer").addClass('show-active-menu');
        setTimeout(function(){
            $(".mask-mini-cart").addClass('mask-menu-active');
        }, 400);

    });
    $("body").delegate( ".mask-mini-cart.mask-menu-active", "click", function() {
        $(".mask-mini-cart").removeClass('mask-menu-active');
        $(".body").removeClass('show-active-menu');
        $(".header").removeClass('show-active-menu');
        $("body").removeClass('body-active');
        $(".footer").removeClass('show-active-menu');
    });

    $(".navPages-container-mobile .container-menu .navPages .navPages-item .next-menu-mobile").click(function(){
       $(this).prev().addClass('navPage-subMenu-active');
        //$(".navPages-container-mobile .container-menu").toggleClass('container-menu-active');
    });
    $(".navPages-container-mobile .prv-menu .prv-menu-mobile").click(function() {
        $(".navPages-container-mobile .container-menu").removeClass('container-menu-active');
        $(".navPages-container-mobile .navPage-subMenu").removeClass('navPage-subMenu-active');
    });

    $(".navPages-container-mobile .navPage-subMenu-item .sub-next-menu-mobile").click(function(){
        $(this).prev().addClass('navPage-childList-mobile-active');
    });

    $(".navPages-container-mobile .navigation-gutter-container .navPage-childList .sub-prv-menu-mobile").click(function(){
       $(".navPages-container-mobile .navPage-childList.navPage-childList-mobile-active").removeClass('navPage-childList-mobile-active');
    });


    $(".tab-sub.product-reviews").click(function(){
        setTimeout(function(){
                var childDivs = $('.productReview');

                for( var i=0; i< childDivs.length; i++ )
                {
                    var a = $(childDivs[i]);
                    var height = a.find(".productReview-body").height();
                    if(height > 80){
                        childDivs[i].getElementsByClassName('show-read-more')[0].className +=" active-read-more";
                    }
                }
        }, 300);

    });

    $(".pages-product #main-product-page .show-read-more").click(function(){
        $(this).prev().toggleClass('show-text-review');
        $(this).children().toggleClass('active-show-text-review');
        $(this).children().next().toggleClass('active-hide-text-review');
    });


    $("body").delegate( ".search-form-menu .quickSearchResults .modal-close", "click", function() {
        $(".search-form-menu .quickSearchResultsWrap.show_menu_search").removeClass('show_menu_search');
    });


    if(!$(".search-form-menu .quickSearchResultsWrap .quick_search_product").hasClass('quick_search_product')){
        $(".header .header-container .logo-search-header .search-form-menu .form-field .form-input").focus(function(){
            $(".search-form-menu .quickSearchResultsWrap").addClass('show_menu_search');
        });
    }

    $("body").delegate( ".search-form-menu .quickSearchResultsWrap .link_search_page a", "click", function() {
        $(".header .header-container .logo-search-header .search-form-menu .form-field button").trigger('click');
    });


// Tabs product
//====================================================================



    $(".container-description-product .tabs-product .tab-description").click(function(){
        $(".container-description-product .tabs-product li").removeClass("active");
        $(this).addClass("active");
        $(".container-description-product .main-show-tabs #tab-description").show();
        $(".container-description-product .main-show-tabs #product-reviews").hide();
        $(".container-description-product .main-show-tabs #warranty").hide();
    });

    $(".container-description-product .tabs-product .product-reviews").click(function(){
        $(".container-description-product .tabs-product li").removeClass("active");
        $(this).addClass("active");
        $(".container-description-product .main-show-tabs #product-reviews").show();
        $(".container-description-product .main-show-tabs #tab-description").hide();
        $(".container-description-product .main-show-tabs #warranty").hide();
    });

    var urlProduct = window.location.href;
    var dataUrlView = $('.content-description-product').data('urlproduct');

    if(urlProduct !=  dataUrlView){
        $(".container-description-product .tabs-product .product-reviews");
        $(".container-description-product .tabs-product li").removeClass("active");
        $(".container-description-product .tabs-product .product-reviews").addClass("active");
        $(".container-description-product .main-show-tabs #product-reviews").show();
        $(".container-description-product .main-show-tabs #tab-description").hide();
        $(".container-description-product .main-show-tabs #warranty").hide();
    }
    else {
        $(".container-description-product .tabs-product li").removeClass("active");
        $(".container-description-product .tabs-product .tab-description").addClass("active");
        $(".container-description-product .main-show-tabs #tab-description").show();
        $(".container-description-product .main-show-tabs #product-reviews").hide();
        $(".container-description-product .main-show-tabs #warranty").hide();
    }


    $(".container-description-product .tabs-product .warranty").click(function(){
        $(".container-description-product .tabs-product li").removeClass("active");
        $(this).addClass("active");
        $(".container-description-product .main-show-tabs #warranty").show();
        $(".container-description-product .main-show-tabs #tab-description").hide();
        $(".container-description-product .main-show-tabs #product-reviews").hide();
    });


    $('.pages-product .right-slide').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        asNavFor: '.left-slide',
        infinite: true,
        prevArrow: $('.prev-button'),
        nextArrow: $('.next-button')
    });

    $(' .pages-product .left-slide').slick({
        slidesToShow: 4,
        arrows: false,
        slidesToScroll: 1,
        asNavFor: '.right-slide',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        infinite: true
    });


}

function sliderBannerHome(){
    $('.slider-banner-homepage .container-slider-home').slick({
        infinite: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
    });

    $('.slider-product-view .right-slide').slick({
        infinite: true,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    });

    $('.slick-fresh-arrivals').slick({
        infinite: true,
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 2,
        speed: 500,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: true,
        responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToScroll: 1,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToScroll: 2,
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 550,
                    settings: {
                        slidesToScroll: 1,
                        slidesToShow: 1
                    }
                }
            ]
    });

    $('.productView .productView-thumbnails-right').slick({
        infinite: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        vertical: true,
        speed: 500,
        arrows: true,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 993,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 4,
                    vertical: false,
                    centerPadding: '10px'
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 4,
                    vertical: false
                }
            },
            {
                breakpoint: 550,
                settings: {
                    slidesToScroll: 1,
                    slidesToShow: 3,
                    vertical: false
                }
            }
        ]
    });


}
/* item from to category */
function loadItem(){
    var page = $("body").hasClass("pages-category");
    if(page){

        if($('.pagination-item .pagination-link').hasClass('pagination-link')){
            var currentPage = $(".pagination-item--current a")[0].innerText,
                fromItem = $(".from-item")[0],
                toItem = $(".to-item")[0],
                numberProduct = $(".product").length;
            toItem.innerText = currentPage * numberProduct;
            if ( currentPage - 1 > 0){
                fromItem.innerText = (currentPage - 1) * numberProduct + 1;
            }

            $(".numbers-product .number-product").each(function () {
                var check = this.innerText;
                if(check == numberProduct){
                    $(this).prop('selected', true)
                }
            })
        }
        var totalProduct = $('.total-product').text();
        if(totalProduct <= 24){
            $('.to-item').text(totalProduct);
            $('.pages-category .pagination-list ').hide();
        }

    }
    $(".card-img-container").each(function () {
        if($(this).children().length >1){
            $(this).addClass("two-image");
        }
    })
}

function hoverMenuActive(){
    $('.navPages-container .navPages-list-nav .navPages-action').hover(function () {

        const $this = $(this);
        const target = $this.data('collapsible');
        var childDivs = $('.navPages-container .navPages-list-menu .navPage-subMenu');

        for( var i=0; i< childDivs.length; i++ )
        {
            var a = $(childDivs[i]);
            var dataSubMenu = a.data('collapsible');
            if(dataSubMenu == target){
                $('.navPages-container .navPages-list-nav .navPages-action').removeClass('active');
                $this.addClass('active');
                childDivs.removeClass('active');
                a.addClass('active');
            }
            if(target == null){
                childDivs.removeClass('active');
                $('.navPages-container .navPages-list-nav .navPages-action').removeClass('active');
            }
        }

    });
    $('.navPages-container .navPages-list-nav .sub-heading-menu').hover(function () {
        $('.navPages-container .navPages-list-nav .navPages-action').removeClass('active');
        $('.navPages-container .navPages-list-menu .navPage-subMenu').removeClass('active');
    })
}

function hoverMenuActiveScroll(){
    $('.search-form-menu .navPages-list-nav .navPages-action').hover(function () {

        const $this = $(this);
        const target = $this.data('collapsible');
        var childDivs = $('.search-form-menu .navPages-list-menu .navPage-subMenu');

        for( var i=0; i< childDivs.length; i++ )
        {
            var a = $(childDivs[i]);
            var dataSubMenu = a.data('collapsible');
            if(dataSubMenu == target){
                $('.search-form-menu .navPages-list-nav .navPages-action').removeClass('active');
                $this.addClass('active');
                childDivs.removeClass('active');
                a.addClass('active');
            }
            if(target == null){
                childDivs.removeClass('active');
                $('.search-form-menu .navPages-list-nav .navPages-action').removeClass('active');
            }
        }

    });
    $('.search-form-menu .navPages-list-nav .sub-heading-menu').hover(function () {
        $('.search-form-menu .navPages-list-nav .navPages-action').removeClass('active');
        $('.search-form-menu .navPages-list-menu .navPage-subMenu').removeClass('active');
    });

    $('.navPages-action').click(function(){
        var dataUrl = $(this).attr('href');
        window.location.replace(dataUrl);
    });

    var hrefSite = window.location.href;
    var childDivs = $('.navPages-menu .navPages-list-nav .navPages-item');

    for( var i=0; i< childDivs.length; i++ ){
        var a = $(childDivs[i]);
        var height1 = a.find(".navPages-action").attr('href');
        if(hrefSite == height1){

            a.find(".navPages-action").toggleClass('active-nav');
        }

    }

    var hrefSubMenu = window.location.href;
    var hrefClick = $('.navPages-menu .navPages-list-menu .navPage-childList-item');

    for( var i=0; i< hrefClick.length; i++ ){
        var a = $(hrefClick[i]);
        var height = a.find(".navPage-childList-action").attr('href');
        if(hrefSubMenu == height){

            a.find(".navPage-childList-action").addClass('active-nav');
            var dataSubMenu = a.parents('.navPage-subMenu').data('collapsible');

            var dataMenu = $('.navPages-menu .navPages-list-nav .navPages-item');

            dataMenu.each(function(){
                var dataLink = $(this).find('.navPages-action').data('collapsible');
                if(dataSubMenu == dataLink){
                    $(this).find('.navPages-action').addClass('active-nav');
                }
            });

        }
    }




    $('.navPages-item').click(function(){
        $('.navPages-item').removeClass('active-nav');
        $(this).addClass('active-nav');
    });

    var count = $('.navUser-item--cart .countPill').text();
    if(count > 1){
        $('.top-link-cart .items-product-cart').show();
        $('.top-link-cart .item-product-cart').hide();
    }else {
        $('.top-link-cart .item-product-cart').show();
        $('.top-link-cart .items-product-cart').hide();
    }


    $('.card-figcaption .card-figcaption-button')

}

